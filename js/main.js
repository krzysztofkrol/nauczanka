bootstrapRevealPassword()

// Plugin to integrate in your javascript file
function bootstrapRevealPassword() {
  $('[data-password]')
    .closest('.form-group')
    .addClass('position-relative')
    .append(
      '<span class="form-password-icon"><i class="far fa-eye"></i></span>',
    )

  $('.form-password-icon').on('click', function () {
    var clicks = $(this).data('clicks')
    if (clicks) {
      // odd clicks
      $(this)
        .html('<i class="far fa-eye"></i>')
        .prevAll(':input')
        .attr('type', 'password')
    } else {
      // even clicks
      $(this)
        .html('<i class="far fa-eye-slash"></i>')
        .prevAll(':input')
        .attr('type', 'text')
    }
    $(this).data('clicks', !clicks)
  })
}

$(document).ready(function () {
  // executes when HTML-Document is loaded and DOM is ready
  $('.slider-intro').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  })
  $('.slider-popular').slick({
    slidesToShow: 5,
    slidesToScroll: 1,

    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  })
  $('.slider-popular-two').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  })
  // $('.slider-calendar').slick({
  //   slidesToShow: 5,
  //   centerMode: true,
  //   responsive: [
  //     {
  //       breakpoint: 1024,
  //       settings: {
  //         slidesToShow: 3,
  //         slidesToScroll: 3,
  //       },
  //     },
  //     {
  //       breakpoint: 600,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 2,
  //       },
  //     },
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1,
  //       },
  //     },
  //   ],
  // })

  // document ready
})

const nav = document.querySelector('#nav-fix')
const navTop = nav.offsetTop

function stickyNavigation() {
  if (window.scrollY - 300 > navTop) {
    nav.classList.add('sticky-top')
  }
  if (window.scrollY < 50) {
    nav.classList.remove('sticky-top')
  }
}
window.addEventListener('scroll', stickyNavigation)
// breakpoint and up

// when you hover a toggle show its dropdown menu
$('.dropdown-toggle').hover(function () {
  $(this).parent().addClass('show')
  $(this).parent().find('.dropdown-menu').addClass('show')
})

// hide the menu when the mouse leaves the dropdown
$('.dropdown-menu').mouseleave(function () {
  $(this).removeClass('show')
})

// do something here

const btnMoreArticles = [...document.querySelectorAll('.articles .btn-more')]
const btnMoreFiles = [...document.querySelectorAll('.files .btn-more')]

const MoreArticles = [...document.querySelectorAll('.articles .more-articles')]
const MoreFiles = [...document.querySelectorAll('.files .more-articles')]

btnMoreArticles.forEach((btnMoreArticles, index) =>
  btnMoreArticles.addEventListener('click', () => {
    MoreArticles[index].classList.toggle('active')
    btnMoreArticles.classList.toggle('active')
  }),
)
btnMoreFiles.forEach((btnMoreFiles, index) =>
  btnMoreFiles.addEventListener('click', () => {
    MoreFiles[index].classList.toggle('active')
    btnMoreFiles.classList.toggle('active')
  }),
)
const CalendarSlides = [...document.querySelectorAll('.slider-calendar .slide')]

let activeCalendar = 2

CalendarSlides.forEach((CalendarSlide, index) =>
  CalendarSlide.addEventListener('click', () => {
    CalendarSlide.classList.toggle('active')

    CalendarSlides[activeCalendar].classList.remove('active')
    activeCalendar = index
  }),
)
